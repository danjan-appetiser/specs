#
# Be sure to run `pod lib lint Notifications-Module.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Notifications-Module'
  s.version          = '0.0.1'
  s.summary          = 'A short description of Notifications-Module.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = "# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!"

  s.homepage         = 'https://github.com/Danjan Pallera/Notifications-Module'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Danjan Pallera' => 'danjan.' }
  s.source           = { :git => 'git@gitlab.com:danjan-appetiser/notifications-pod.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '14.0'

  s.source_files = 'Notifications-Module/Classes/**/*'
  s.resources = "Notifications-Module/Assets/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"

  # s.resource_bundles = {
  #   'Notifications-Module' => ['Notifications-Module/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  s.dependency 'Core-Module', '0.1.1'
end
