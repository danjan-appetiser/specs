#
#  Be sure to run `pod spec lint CoreModule.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  spec.name         = "CoreModule"
  spec.version      = "0.0.1"
  spec.summary      = "A short description of CoreModule."

  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  spec.description  = "Core Module"

  spec.homepage     = "http://EXAMPLE/CoreModule"
  # spec.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"


  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Licensing your code is important. See https://choosealicense.com for more info.
  #  CocoaPods will detect a license file if there is a named LICENSE*
  #  Popular ones are 'MIT', 'BSD' and 'Apache License, Version 2.0'.
  #

  spec.license      = "MIT"
  # spec.license      = { :type => "MIT", :file => "FILE_LICENSE" }


  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the authors of the library, with email addresses. Email addresses
  #  of the authors are extracted from the SCM log. E.g. $ git log. CocoaPods also
  #  accepts just a name if you'd rather not provide an email address.
  #
  #  Specify a social_media_url where others can refer to, for example a twitter
  #  profile URL.
  #

  spec.author             = { "Danjan Pallera" => "danjan." }
  # Or just: spec.author    = "Danjan Pallera"
  # spec.authors            = { "Danjan Pallera" => "danjan." }
  # spec.social_media_url   = "https://twitter.com/Danjan Pallera"

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If this Pod runs only on iOS or OS X, then specify the platform and
  #  the deployment target. You can optionally include the target after the platform.
  #

  spec.platform     = :ios
  spec.platform     = :ios, "15.0"
  
  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the location from where the source should be retrieved.
  #  Supports git, hg, bzr, svn and HTTP.
  #

  spec.source       = {
  :git => "git@gitlab.com:danjan-appetiser/core-module.git",
  :tag => '0.0.1'
}


  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  CocoaPods is smart about how it includes source code. For source files
  #  giving a folder will include any swift, h, m, mm, c & cpp files.
  #  For header files it will include any header in the folder.
  #  Not including the public_header_files will make all headers public.
  #

spec.source_files  = "CoreModule/**/**.{h,m,swift}"

  # spec.public_header_files = "Classes/**/*.h"


  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  A list of resources included with the Pod. These are copied into the
  #  target bundle with a build phase script. Anything else will be cleaned.
  #  You can preserve files from being cleaned, please don't preserve
  #  non-essential files like tests, examples and documentation.
  #

  # spec.resource  = "icon.png"
# spec.resources = "CoreModule/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"

  # spec.preserve_paths = "FilesToSave", "MoreFilesToSave"


  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  # spec.framework  = "SomeFramework"
  # spec.frameworks = "SomeFramework", "AnotherFramework"

  # spec.library   = "iconv"
  # spec.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # spec.requires_arc = true

  # spec.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # spec.dependency "JSONKit", "~> 1.4"

  spec.dependency 'Alamofire', '4.8.2'
  spec.dependency 'AlamofireImage', '~> 3.5.2'
  spec.dependency 'SwiftDate', '~> 5.1.0'
  spec.dependency 'SkeletonView', '~> 1.8.7'
  spec.dependency 'SVProgressHUD', '~> 2.2.5'
  spec.dependency 'PureLayout', '~> 3.1.8'
  spec.dependency 'MaterialComponents/Buttons', '~> 104.0.1'
  spec.dependency 'MaterialComponents/Buttons+Theming', '~> 104.0.1'
  spec.dependency 'MaterialComponents/TextFields', '~> 104.0.1'
  spec.dependency 'MaterialComponents/TextFields+Theming', '~> 104.0.1'
  spec.dependency 'MaterialComponents/Snackbar', '~> 104.0.1'
  spec.dependency 'PhoneNumberKit', '3.3.3'
  spec.dependency 'R.swift', '~> 5.4.0'
  spec.dependency 'Valet', '~> 4.1.1'
  spec.dependency 'RxSwift', '5.1.0'
  spec.dependency 'RxCocoa', '5.1.0'
  spec.dependency 'RxSwiftExt', '5.1.0'
  spec.dependency 'NSObject+Rx', '5.1.0'
end
